import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:venue_booking/Models/user.dart';
import 'package:venue_booking/Modules/Blocks/blocProvider.dart';
import 'package:venue_booking/Modules/MobileNumberValidation/add_mobile_bloc.dart';
import 'package:venue_booking/config.dart';

class VerifyOtpApi {
  final BuildContext context;

  VerifyOtpApi(this.context);
  final Client _client = Client();
  String _verifyOtpUrl = Uri.encodeFull(
      Config.baseUrl + 'user/verify_otp/?phone={phone}&password={password}');
  Future<VerifyOTPResponse> verifyOtp({String mobile, String password}) async {
    _onTimeout() {
      ApiStatus(status: APIStatus.error);
    }

    final loadingBloc = Provider.of<LoadingControllerBloc>(context);
    loadingBloc.updateLoaderStatus(
        status: ApiStatus(status: APIStatus.waiting));
    String test = Uri.parse(_verifyOtpUrl
            .replaceAll('{phone}', '+91' + mobile)
            .replaceAll('{password}', password))
        .toString();
    print(test);
    return await _client
        .get(
          test,
          //headers: {"Content-Type": "application/json"}
        )
        .timeout(const Duration(seconds: 60), onTimeout: _onTimeout)
        .then((Response res) => res.body)
        .then(json.decode)
        .then((json) => VerifyOTPResponse.fromJson(json))
        .whenComplete(() {
      loadingBloc.updateLoaderStatus(status: ApiStatus(status: APIStatus.done));
    }).catchError((error) {
      loadingBloc.updateLoaderStatus(
          status: ApiStatus(status: APIStatus.error));
      print('error : $error');
    });
  }
}

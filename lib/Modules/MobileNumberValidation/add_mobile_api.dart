import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:venue_booking/Modules/Blocks/blocProvider.dart';
import 'package:venue_booking/Modules/MobileNumberValidation/add_mobile_bloc.dart';
import 'package:venue_booking/config.dart';

class AddMobileApi {
  final String mobileNumber;
  final BuildContext context;
  AddMobileApi({this.mobileNumber, this.context});

  final Client _client = Client();

  String _generateOtpUrl =
      Uri.encodeFull(Config.baseUrl + 'user/get_otp/?phone={phone}');

  Future<int> generateOtp() async {
    _onTimeout() {
      ApiStatus(status: APIStatus.error);
    }

    final loadingBloc = Provider.of<LoadingControllerBloc>(context);
    loadingBloc.updateLoaderStatus(
        status: ApiStatus(status: APIStatus.waiting));
    return await _client
        .get(Uri.parse(
            _generateOtpUrl.replaceAll('{phone}', '+91' + mobileNumber)))
        .timeout(const Duration(seconds: 2), onTimeout: _onTimeout)
        .then((Response res) => res.statusCode)
        .whenComplete(() {
      loadingBloc.updateLoaderStatus(status: ApiStatus(status: APIStatus.done));
    }).catchError((error) {
      loadingBloc.updateLoaderStatus(
          status: ApiStatus(status: APIStatus.error));
      print('error : $error');
    });
  }
}

import 'package:rxdart/rxdart.dart';
import 'package:venue_booking/Modules/validators/validators.dart';

class AddMobileBloc extends Object with Validators {
  final _mobileNumberController = BehaviorSubject<String>();
  Function(String) get mobileNumberChanged => _mobileNumberController.sink.add;
  Observable<String> get mobileNumber =>
      _mobileNumberController.stream.transform(mobileNumberValidator);
  Observable<bool> get submitCheck =>
      Observable.combineLatest2(mobileNumber, mobileNumber, (m, n) => true);
//  BehaviorSubject<bool> _loadingController =
//      BehaviorSubject<bool>(seedValue: false);
//  Observable<bool> get isLoadingStream => _loadingController.stream;
  void dispose() {
    print('disposed');

//    _loadingController.close();
    _mobileNumberController.close();
  }
}

abstract class BaseBloc {
  void dispose();
}

class LoadingControllerBloc {
  final _loadingController = BehaviorSubject<ApiStatus>();
  updateLoaderStatus({status: ApiStatus}) {
    _loadingController.sink.add(status);
  }

  Sink<ApiStatus> get loadingStatus => _loadingController.sink;
  Observable<ApiStatus> get isLoadingStream => _loadingController.stream;
  void dispose() {
    print('disposed');
    _loadingController.close();
  }
}

class ApiStatus {
  final APIStatus status;
  ApiStatus({this.status});
}

enum APIStatus { waiting, done, error }
